import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    collections: [],
  },
  getters: {
    getCollections: (state) => state.users,
  },
  actions: {
    SET_COLLECTIONS(state, collections) {
      state.collections = collections;
    }
  },
  mutations: {
    fetchCollection({ commit }) {
      axios.get("https://staging-api.bloobloom.com/user/v1/sales_channels/website/collections/")
        .then(response => {
          console.log({ response })
          commit("SET_COLLECTIONS", response.data);
        })
  },
  },
  modules: {
  }
})

